const express = require('express');

const app = express();
app.use(express.json());

app.get('/results/:n1/:n2',(req,res)=>{
    const n1 = parseInt(req.params.n1);
    const n2 = parseInt(req.params.n2);
    const result = n1+n2;
    res.send(`Suma de ${n1} + ${n2}: ${result}`);
});

app.post('/results',(req,res)=>{
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req.body.n2);
    const result = n1 * n2;
    res.send(`Multiplicación de ${n1} * ${n2} es ${result}`);
});

app.put('/results',(req,res)=>{
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req,body.n2);
    const result = n1/n2;
    res.send(`division de ${n1} / ${n2} ${result}`);
});

app.patch("/results",(req,res)=>{
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req.body.n2);
    const result = (n1**n2);
    res.send(`potencia de ${n1} ^ ${n2} ${result}`);
});

app.delete("/results:/n2/:n2",(req,res)=>{
    const n1 = parseInt(req.body.n1);
    const n2 = parseInt(req.body.n2);
    const result = (n1-n2);
    res.send(`resta de ${n1} - ${n2} ${result}`);
});

app.listen(8000,()=>{
    console.log("hello you are in 8000 port");
});